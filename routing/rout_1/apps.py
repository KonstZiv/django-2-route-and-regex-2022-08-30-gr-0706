from django.apps import AppConfig


class Rout1Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'rout_1'
